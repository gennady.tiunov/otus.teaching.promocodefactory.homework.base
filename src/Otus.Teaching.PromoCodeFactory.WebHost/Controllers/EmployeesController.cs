﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(
            IRepository<Employee> employeeRepository,
            IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<EmployeeShortResponse>>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return Ok(employeesModelList);
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromoCodesCount = employee.AppliedPromoCodesCount
            };

            return Ok(employeeModel);
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> AddEmployee(EmployeeRequest employeeModel)
        {
            if (await _employeeRepository.GetByIdAsync(employeeModel.Id) != null)
                return Conflict();

            var roles = (await _rolesRepository.GetAllAsync()).ToList();

            var roleIds = employeeModel.RoleIds.ToList();
            foreach (var roleId in roleIds)
            {
                if (roles.All(p => p.Id != roleId))
                    return BadRequest();
            }

            var employee = new Employee
            {
                FirstName= employeeModel.FirstName,
                LastName = employeeModel.LastName,
                Email = employeeModel.Email,
                Roles = roles.Where(p => roleIds.Contains(p.Id)).ToList()
            };

            await _employeeRepository.AddAsync(employee);

            var url = $"{ControllerContext.HttpContext.Request.Scheme}://{ControllerContext.HttpContext.Request.Host}/api/v1/Employees/{employee.Id}";

            return Created(url, employee.Id);
        }

        /// <summary>
        /// Изменить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateEmployee(EmployeeRequest employeeModel)
        {
            var employee = await _employeeRepository.GetByIdAsync(employeeModel.Id);
            if (employee == null)
                return NotFound();

            var roles = (await _rolesRepository.GetAllAsync()).ToList();

            var roleIds = employeeModel.RoleIds.ToList();
            foreach (var roleId in roleIds)
            {
                if (roles.All(p => p.Id != roleId))
                    return BadRequest();
            }

            employee.FirstName = employeeModel.FirstName;
            employee.LastName = employeeModel.LastName;
            employee.Email = employeeModel.Email;
            employee.Roles = roles.Where(p => roleIds.Contains(p.Id)).ToList();

            await _employeeRepository.UpdateAsync(employee);

            return Ok();
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteEmployee(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                return NotFound();

            await _employeeRepository.DeleteAsync(id);

            return NoContent();
        }
    }
}